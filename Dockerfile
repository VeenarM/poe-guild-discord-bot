FROM node:current-alpine3.21

# Create app directory
RUN mkdir -p /home/node/app/node_modules /home/node/app/logs && chown -R node:node /home/node/app /home/node/app/logs

WORKDIR /home/node/app

# Install app dependencies
COPY --chown=node:node package*.json ./

USER node

RUN npm ci

# Bundle app source
COPY --chown=node:node src src

CMD [ "npm", "start" ]
