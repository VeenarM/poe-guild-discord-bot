const valvelet = require('valvelet');
const logger = require('../logger').getLogger();
const version = require('../../package').version;

const opts = {
  json: true,
  headers: {
    'User-Agent': `NodeJS node-fetch poe-guild-stash/${version} (Gitlab/@VeenarM)`,
    cookie: `POESESSID=${process.env.POESESSID};`
  }
}

async function getGuildProfile() {
  let encodedUrl = encodeURI(`https://www.pathofexile.com/guild/profile/${process.env.GUILD_PROFILE_ID}`);
  logger.debug(`Calling: ${encodedUrl}`);
  return await fetch(encodedUrl, opts).then((res) => {
    logger.debug(`Status: ${res.status}`);
    return res.text();
  });
}

async function getPatchNotes() {
  let url = `https://www.pathofexile.com/forum/view-forum/patch-notes`;
  logger.debug(`Calling: ${url}`);
  return await fetch(url, opts).then((res) => {
    logger.debug(`Status: ${res.status}`);
    return res.text();
  });
}

/**
 * Rate Limited Function (Has it's own limiter)
 * x-rate-limit-policy: ladder-view
 * x-rate-limit-ip: 5:5:10,10:10:30,15:10:300
 * X requests per X seconds, if go over = X second ban.
 * 5:5:10 -> Ban 10 seconds
 * 10:10:30 -> Ban 30 seconds
 * 15:10:300 -> Ban 300 seconds 
 * @param {} accountName 
 */
const getLeagueLadderForAccount = valvelet(getLeagueLadderForAccountLimited, 1, 1010);

async function getLeagueLadderForAccountLimited(currentLeague, accountName) {
  let encodedUrl = encodeURI(`https://api.pathofexile.com/ladders/${currentLeague}?accountName=${accountName}`);
  logger.debug(`Calling: ${encodedUrl}`);
  return await fetch(encodedUrl, opts).then((res) => {
    logger.debug(`Status: ${res.status}`);
    return res.json();
  });

}

/**
 * Rate Limited Function x per/x sec/else x sec ban
 * x-rate-limit-account: 60:60:60,200:120:900
 * x-rate-limit-account-state: 1:60:0,4:120:0
 * x-rate-limit-policy: backend-character-request-limit
 * x-rate-limit-rules: Account
 * @param {} accountName 
 */
const getAccountCharacters = valvelet(getAccountCharactersCall, 1, 2000);

async function getAccountCharactersCall(accountName) {
  let encodedUrl = encodeURI(`https://www.pathofexile.com/character-window/get-characters?accountName=${accountName}`);
  logger.debug(`Calling: ${encodedUrl}`);
  return await fetch(encodedUrl, opts).then((res) => {
    logger.debug(`Status: ${res.status}`);
    return res.json();
  });
}

/**
 * Rate Limited Function x per/x sec/else x sec ban
 * x-rate-limit-account: 60:60:60,200:120:900
 * x-rate-limit-account-state: 1:60:0,1:120:0
 * x-rate-limit-policy: backend-character-request-limit
 * x-rate-limit-rules: Account
 * @param {*} characterName 
 */
const fetchAccountNameForCharacter = valvelet(fetchAccountNameForCharacterCall, 1, 1010);

async function fetchAccountNameForCharacterCall(characterName) {
  let encodedUrl = encodeURI(`https://www.pathofexile.com/character-window/get-account-name-by-character?character=${characterName}`);
  logger.debug(`Calling: ${encodedUrl}`);
  return await fetch(encodedUrl, opts).then((res) => {
    logger.debug(`Status: ${res.status}`);
    return res.json();
  });
}

module.exports = {
  getPatchNotes,
  getLeagueLadderForAccount,
  getAccountCharacters,
  getGuildProfile,
  fetchAccountNameForCharacter
}
